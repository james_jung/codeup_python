h, b, c, s = input().split()
h = int(h)
b = int(b)
c = int(c)
s = int(s)

def size(h, b, c, s):
    size = (h*b*c*s)/8/1024/1024
    size = round(size, 1)
    print(size, 'MB', sep=' ')

size(h, b, c, s)