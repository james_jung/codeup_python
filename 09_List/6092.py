n = int(input())
a = input().split() # 공백을 기준으로 리스트 a에 입력값을 저장

# 리스트 내의 값을 정수로 변환
for i in range(n):
    a[i] = int(a[i])

# 카운트 리스트 생성
d = []
d = [0] * 24

# 카운트 될 때마다 +1 해주기
for i in range(n):
    d[a[i]] += 1

# 공백을 두고 출력
for i in range(1, 24):
    print(d[i], end=' ')