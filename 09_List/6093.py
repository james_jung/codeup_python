n = int(input()) # 출석 부른 횟수
k = input().split() # 공백으로 구분된 출석 번호

# 입력된 리스트를 정수화
for i in range(n):
    k[i] = int(k[i])

for i in range(n-1, -1, -1):
    print(k[i], end=' ')

