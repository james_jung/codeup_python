n = int(input()) # 10이하의 자연수

# 2차원 리스트 생성하고 초기값 0로 세팅
d = []
for i in range(20):
    d.append([])
    for j in range(20):
        d[i].append(0)

# 공백으로 입력된 좌표의 값을 1로 변경
for i in range(n):
    x, y = input().split()
    d[int(x)][int(y)] = 1

# 2차원 리스트 출력
for i in range(1,20):
    for j in range(1,20):
        print(d[i][j], end=' ')
    print()